﻿// For more information see https://aka.ms/fsharp-console-apps
open Elmish
open Elmish.React
open Feliz
open System

type Pic = {id:int;pic_name:string;mutable paired:bool;mutable numpressed:int}

type State =
    |   FirstOpen of Timer:int * PicId_First:int * Points:int
    |   SecondOpen  of Timer:int * PicId_First:int * PicId_Second:int * Points:int
    |   GameOver of Points:int
    |   Begin of Timer:int * Points:int

type Message = 
    |   Timer_msg
    |   OpenPic of PicId:int
    |   Restart 
    |   FinshedSleeping


//=======================================================================
//POMOCNE FUNKCIJE ZA UPDATE FUNKCIJU
//=======================================================================
let LowerTimerFor_1 dispatch = async {
    do! Async.Sleep 1000
    dispatch Timer_msg
}
let Sleep_for_1500 () =async{ 
    do! Async.Sleep 1500
    return FinshedSleeping
}
let Sleep_for_750 () =async{ 
    do! Async.Sleep 750
    return FinshedSleeping
}
let Sleep_for_amount (a:int) dispatch=async{ 
    do! Async.Sleep a
    dispatch FinshedSleeping
}
let cmdToTimer dispatch=
    Async.Start(computation=LowerTimerFor_1 dispatch)

let init () = 
    Begin (30,0), Cmd.ofEffect cmdToTimer

let mutable Pictures :List<Pic>=[
    {id=0;pic_name="images/hippo.jpg";paired=false;numpressed=0}
    {id=1;pic_name="images/croc.jpg";paired=false;numpressed=0}
    {id=2;pic_name="images/lion.jpg";paired=false;numpressed=0}
    {id=3;pic_name="images/elephant.jpg";paired=false;numpressed=0}
    {id=4;pic_name="images/dog.jpg";paired=false;numpressed=0}
    {id=5;pic_name="images/lion.jpg";paired=false;numpressed=0}
    {id=6;pic_name="images/hippo.jpg";paired=false;numpressed=0}
    {id=7;pic_name="images/croc.jpg";paired=false;numpressed=0}
    {id=8;pic_name="images/dolphin.jpg";paired=false;numpressed=0}
    {id=9;pic_name="images/dog.jpg";paired=false;numpressed=0}
    {id=10;pic_name="images/dolphin.jpg";paired=false;numpressed=0}
    {id=11;pic_name="images/elephant.jpg";paired=false;numpressed=0}
] 

//=======================================================================
//POMOCNE FUNKCIJE ZA UPDATE FUNKCIJU
//=======================================================================
let randomize () =
    let r=System.Random()
    Pictures <- Pictures|>List.sortBy(fun _ ->r.Next())
randomize()
randomize()
let isPair (a:Pic,b:Pic)=
    if a.pic_name=b.pic_name then true else false

let CheckForWin ():bool=
    let mutable pom=true
    for i in [0..11] do
        if Pictures[i].paired=false then pom<-false
    pom

let PointCalculator (pic1:Pic)=
    let pom=pic1.numpressed
    if pom<3 then 10
    elif pom=3 then 5
    elif pom=4 then 2
    else 0

let update (msg:Message) (state:State) : State * Cmd<'a> =
    match state with
    | Begin (t,p)->
        if t=0 || CheckForWin() then (GameOver p,Cmd.none) else
        match msg with
        |Timer_msg-> Begin (t-1,p),Cmd.ofEffect cmdToTimer
        |OpenPic pic_id-> 
            Pictures[pic_id].numpressed<-Pictures[pic_id].numpressed+1
            FirstOpen (t,pic_id,p),Cmd.none
        |_->state,Cmd.none

    | FirstOpen (t,pic_id,p) ->
        if t=0 then (GameOver p,Cmd.none) else
        match msg with
        |Timer_msg->FirstOpen(t-1,pic_id,p),Cmd.ofEffect cmdToTimer
        |OpenPic pic_id2->
            if pic_id=pic_id2 then state,Cmd.none 
            else
                Pictures[pic_id2].numpressed<-Pictures[pic_id2].numpressed+1
                if isPair (Pictures[pic_id],Pictures[pic_id2]) then
                    SecondOpen(t,pic_id,pic_id2,p),Cmd.OfAsync.perform Sleep_for_1500 ()id
                else
                    SecondOpen(t,pic_id,pic_id2,p),Cmd.OfAsync.perform Sleep_for_750 ()id
        |_->state,Cmd.none

    | SecondOpen(t,pic_id1,pic_id2,p)->
        if t=0 then (GameOver p,Cmd.none) 
        else     
                match msg with
                | FinshedSleeping->
                    if isPair (Pictures[pic_id1],Pictures[pic_id2]) then
                        let p1=PointCalculator Pictures[pic_id1]
                        let p2=PointCalculator Pictures[pic_id2]

                        Pictures[pic_id1].paired<-true
                        Pictures[pic_id2].paired<-true  
                        Begin (t,p+p1+p2),Cmd.none 
                    else
                        Begin (t,p),Cmd.none   
                | Timer_msg->SecondOpen(t-1,pic_id1,pic_id2,p),Cmd.ofEffect cmdToTimer
                | _->state,Cmd.none

    | GameOver p->
        for i in [0..11] do 
            Pictures[i].paired<-false
            Pictures[i].numpressed<-0
        match msg with
        |Restart -> 
            randomize ()
            randomize ()
            Begin(30,0),Cmd.ofEffect cmdToTimer 
        |_ ->       state,Cmd.none
//=======================================================================
//POMOCNE FUNKCIJE ZA RENDER FUNKCIJU
//=======================================================================
let picDisplay (id:int) (state:State) dispatch : ReactElement = 
    let pic = Pictures[id]
    Html.img [
        if pic.paired =false then
            prop.style [
                style.margin 10;
                style.width 110;
                style.height 110;
                style.visibility.visible;         
            ]
            prop.onClick (fun me -> dispatch (OpenPic id))
        else    
            prop.style[
                style.margin 10;
                style.width 110;
                style.height 110;
                style.visibility.hidden
                ]
        match state with
        | Begin _ -> 
            if pic.paired then "" else "images/test.png"
        | FirstOpen (_, pic_id1,_) ->
            if pic.paired then ""
            elif pic_id1 = id then pic.pic_name else "images/test.png"
        | SecondOpen (_, pic_id1, pic_id2,_) ->
            if pic.paired then "" 
            elif pic_id1 = id || pic_id2 = id then pic.pic_name else "images/test.png" 
        | _ -> ""
        |> prop.src 
    ]

let display (state:State) (t:int)(p:int) dispatch =
    Html.div [
            prop.style [
                style.justifyContent.center
                style.display.flex
                style.flexDirection.column
                style.alignItems.center
            ];
            prop.children [
                Html.div [
                    prop.style [
                        style.justifyContent.center
                        style.display.flex
                        style.flexDirection.column
                        style.alignItems.center
                        style.backgroundColor "#2B304F"
                        style.borderStyle.double
                        style.borderColor "white"
                        style.borderWidth 6
                        style.borderRadius 5
                        style.width 400
                        style.height 100
                        style.color "white"
                        style.fontFamily "Georgia"
                        style.fontSize 25
                    ]               
                    prop.children [
                        Html.label[
                            ("Preostalo vremena: "+ string t)|>prop.text
                        ]
                        Html.label[
                            ("Osvojeni bodovi: "+ string p)|>prop.text
                        ]
                    ]
                ]
                Html.br [];
                Html.div [
                    prop.style [
                        style.width 600
                        style.height 500
                        style.justifyContent.center
                        style.display.flex
                        style.alignItems.center
                    ]
                    prop.children [
                        Html.div [
                            prop.children [
                                picDisplay 0 state dispatch; picDisplay 1 state dispatch; picDisplay 2 state dispatch; picDisplay 3 state dispatch; Html.br[]
                                picDisplay 4 state dispatch; picDisplay 5 state dispatch; picDisplay 6 state dispatch; picDisplay 7 state dispatch; Html.br[]
                                picDisplay 8 state dispatch; picDisplay 9 state dispatch; picDisplay 10 state dispatch; picDisplay 11 state dispatch; Html.br[]
                            ]
                        ]

                    ]
                ]

            ]
        ]

let GameOverDisplayOnly (state:State) (p:int) dispatch=
    Html.div [
                    prop.style [
                        style.alignItems.center
                        style.justifyContent.center
                        style.display.flex
                    ]
                    prop.children [
                        Html.div[
                            
                            prop.style [
                                style.justifyContent.center
                                style.display.flex
                                style.flexDirection.column
                                style.alignItems.center
                                style.backgroundColor "#2B304F"
                                style.borderStyle.double
                                style.borderColor "white"
                                style.borderWidth 6
                                style.borderRadius 5
                                style.margin 100;
                                style.width 450
                                style.height 80;
                                style.color "white"
                                style.fontFamily "Georgia"
                                style.fontSize 35
                            ]
                            ("Ostvareni bodovi: " + string p)|>prop.text
                        ]
                        Html.br []
                        Html.br []
                        Html.button [
                            prop.style [
                                style.justifyContent.center
                                style.display.flex
                                style.alignItems.center
                                style.backgroundColor "#2B304F"
                                style.color "white"
                                style.fontFamily "Georgia"
                                style.borderRadius 15
                                style.fontSize 20
                                style.height 90
                                style.width 150 
                            ]
                            prop.text "Zaigraj ponovo";
                            prop.onClick (fun me -> dispatch Restart)
                        ] 
                    ]
                ]  

let render state dispatch =  
    match state with        
    | Begin (t,p) ->   display state t p dispatch
    | FirstOpen (t, pic_id1,p) ->   display state t p dispatch
    | SecondOpen (t, pic_id1, pic_id2,p) ->    display state t p dispatch
    | GameOver p->    GameOverDisplayOnly state p dispatch                                           
        
 
Program.mkProgram init update render
|> Program.withReactSynchronous "app"
|> Program.run 


