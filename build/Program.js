import { Union, Record } from "./fable_modules/fable-library.4.9.0/Types.js";
import { union_type, record_type, bool_type, string_type, int32_type } from "./fable_modules/fable-library.4.9.0/Reflection.js";
import { singleton } from "./fable_modules/fable-library.4.9.0/AsyncBuilder.js";
import { start, sleep } from "./fable_modules/fable-library.4.9.0/Async.js";
import { Cmd_none, Cmd_ofEffect } from "./fable_modules/Fable.Elmish.4.0.0/cmd.fs.js";
import { int32ToString, createObj, disposeSafe, getEnumerator, comparePrimitives, createAtom } from "./fable_modules/fable-library.4.9.0/Util.js";
import { singleton as singleton_2, item, sortBy, ofArray } from "./fable_modules/fable-library.4.9.0/List.js";
import { nonSeeded } from "./fable_modules/fable-library.4.9.0/Random.js";
import { singleton as singleton_1, append, delay, toList } from "./fable_modules/fable-library.4.9.0/Seq.js";
import { rangeDouble } from "./fable_modules/fable-library.4.9.0/Range.js";
import { Cmd_OfAsync_start, Cmd_OfAsyncWith_perform } from "./fable_modules/Fable.Elmish.4.0.0/cmd.fs.js";
import { createElement } from "react";
import { Interop_reactApi } from "./fable_modules/Feliz.2.7.0/Interop.fs.js";
import { ProgramModule_mkProgram, ProgramModule_run } from "./fable_modules/Fable.Elmish.4.0.0/program.fs.js";
import { Program_withReactSynchronous } from "./fable_modules/Fable.Elmish.React.4.0.0/react.fs.js";

export class Pic extends Record {
    constructor(id, pic_name, paired, numpressed) {
        super();
        this.id = (id | 0);
        this.pic_name = pic_name;
        this.paired = paired;
        this.numpressed = (numpressed | 0);
    }
}

export function Pic_$reflection() {
    return record_type("Program.Pic", [], Pic, () => [["id", int32_type], ["pic_name", string_type], ["paired", bool_type], ["numpressed", int32_type]]);
}

export class State extends Union {
    constructor(tag, fields) {
        super();
        this.tag = tag;
        this.fields = fields;
    }
    cases() {
        return ["FirstOpen", "SecondOpen", "GameOver", "Begin"];
    }
}

export function State_$reflection() {
    return union_type("Program.State", [], State, () => [[["Timer", int32_type], ["PicId_First", int32_type], ["Points", int32_type]], [["Timer", int32_type], ["PicId_First", int32_type], ["PicId_Second", int32_type], ["Points", int32_type]], [["Points", int32_type]], [["Timer", int32_type], ["Points", int32_type]]]);
}

export class Message extends Union {
    constructor(tag, fields) {
        super();
        this.tag = tag;
        this.fields = fields;
    }
    cases() {
        return ["Timer_msg", "OpenPic", "Restart", "FinshedSleeping"];
    }
}

export function Message_$reflection() {
    return union_type("Program.Message", [], Message, () => [[], [["PicId", int32_type]], [], []]);
}

export function LowerTimerFor_1(dispatch) {
    return singleton.Delay(() => singleton.Bind(sleep(1000), () => {
        dispatch(new Message(0, []));
        return singleton.Zero();
    }));
}

export function Sleep_for_1500() {
    return singleton.Delay(() => singleton.Bind(sleep(1500), () => singleton.Return(new Message(3, []))));
}

export function Sleep_for_750() {
    return singleton.Delay(() => singleton.Bind(sleep(750), () => singleton.Return(new Message(3, []))));
}

export function Sleep_for_amount(a, dispatch) {
    return singleton.Delay(() => singleton.Bind(sleep(a), () => {
        dispatch(new Message(3, []));
        return singleton.Zero();
    }));
}

export function cmdToTimer(dispatch) {
    start(LowerTimerFor_1(dispatch));
}

export function init() {
    return [new State(3, [30, 0]), Cmd_ofEffect((dispatch) => {
        cmdToTimer(dispatch);
    })];
}

export let Pictures = createAtom(ofArray([new Pic(0, "images/hippo.jpg", false, 0), new Pic(1, "images/croc.jpg", false, 0), new Pic(2, "images/lion.jpg", false, 0), new Pic(3, "images/elephant.jpg", false, 0), new Pic(4, "images/dog.jpg", false, 0), new Pic(5, "images/lion.jpg", false, 0), new Pic(6, "images/hippo.jpg", false, 0), new Pic(7, "images/croc.jpg", false, 0), new Pic(8, "images/dolphin.jpg", false, 0), new Pic(9, "images/dog.jpg", false, 0), new Pic(10, "images/dolphin.jpg", false, 0), new Pic(11, "images/elephant.jpg", false, 0)]));

export function randomize() {
    const r = nonSeeded();
    Pictures(sortBy((_arg) => r.Next0(), Pictures(), {
        Compare: comparePrimitives,
    }));
}

randomize();

randomize();

export function isPair(a, b) {
    if (a.pic_name === b.pic_name) {
        return true;
    }
    else {
        return false;
    }
}

export function CheckForWin() {
    let pom = true;
    const enumerator = getEnumerator(toList(rangeDouble(0, 1, 11)));
    try {
        while (enumerator["System.Collections.IEnumerator.MoveNext"]()) {
            const i = enumerator["System.Collections.Generic.IEnumerator`1.get_Current"]() | 0;
            if (item(i, Pictures()).paired === false) {
                pom = false;
            }
        }
    }
    finally {
        disposeSafe(enumerator);
    }
    return pom;
}

export function PointCalculator(pic1) {
    const pom = pic1.numpressed | 0;
    if (pom < 3) {
        return 10;
    }
    else {
        switch (pom) {
            case 3:
                return 5;
            case 4:
                return 2;
            default:
                return 0;
        }
    }
}

export function update(msg, state) {
    switch (state.tag) {
        case 0: {
            const t_1 = state.fields[0] | 0;
            const pic_id_1 = state.fields[1] | 0;
            const p_1 = state.fields[2] | 0;
            if (t_1 === 0) {
                return [new State(2, [p_1]), Cmd_none()];
            }
            else {
                switch (msg.tag) {
                    case 0:
                        return [new State(0, [t_1 - 1, pic_id_1, p_1]), Cmd_ofEffect((dispatch_1) => {
                            cmdToTimer(dispatch_1);
                        })];
                    case 1: {
                        const pic_id2 = msg.fields[0] | 0;
                        if (pic_id_1 === pic_id2) {
                            return [state, Cmd_none()];
                        }
                        else {
                            item(pic_id2, Pictures()).numpressed = ((item(pic_id2, Pictures()).numpressed + 1) | 0);
                            if (isPair(item(pic_id_1, Pictures()), item(pic_id2, Pictures()))) {
                                return [new State(1, [t_1, pic_id_1, pic_id2, p_1]), Cmd_OfAsyncWith_perform((x_1) => {
                                    Cmd_OfAsync_start(x_1);
                                }, Sleep_for_1500, void 0, (x) => x)];
                            }
                            else {
                                return [new State(1, [t_1, pic_id_1, pic_id2, p_1]), Cmd_OfAsyncWith_perform((x_3) => {
                                    Cmd_OfAsync_start(x_3);
                                }, Sleep_for_750, void 0, (x_2) => x_2)];
                            }
                        }
                    }
                    default:
                        return [state, Cmd_none()];
                }
            }
        }
        case 1: {
            const t_2 = state.fields[0] | 0;
            const pic_id2_1 = state.fields[2] | 0;
            const pic_id1 = state.fields[1] | 0;
            const p_2 = state.fields[3] | 0;
            if (t_2 === 0) {
                return [new State(2, [p_2]), Cmd_none()];
            }
            else {
                switch (msg.tag) {
                    case 3:
                        if (isPair(item(pic_id1, Pictures()), item(pic_id2_1, Pictures()))) {
                            const p1 = PointCalculator(item(pic_id1, Pictures())) | 0;
                            const p2 = PointCalculator(item(pic_id2_1, Pictures())) | 0;
                            item(pic_id1, Pictures()).paired = true;
                            item(pic_id2_1, Pictures()).paired = true;
                            return [new State(3, [t_2, (p_2 + p1) + p2]), Cmd_none()];
                        }
                        else {
                            return [new State(3, [t_2, p_2]), Cmd_none()];
                        }
                    case 0:
                        return [new State(1, [t_2 - 1, pic_id1, pic_id2_1, p_2]), Cmd_ofEffect((dispatch_2) => {
                            cmdToTimer(dispatch_2);
                        })];
                    default:
                        return [state, Cmd_none()];
                }
            }
        }
        case 2: {
            const p_3 = state.fields[0] | 0;
            const enumerator = getEnumerator(toList(rangeDouble(0, 1, 11)));
            try {
                while (enumerator["System.Collections.IEnumerator.MoveNext"]()) {
                    const i = enumerator["System.Collections.Generic.IEnumerator`1.get_Current"]() | 0;
                    item(i, Pictures()).paired = false;
                    item(i, Pictures()).numpressed = 0;
                }
            }
            finally {
                disposeSafe(enumerator);
            }
            if (msg.tag === 2) {
                randomize();
                randomize();
                return [new State(3, [30, 0]), Cmd_ofEffect((dispatch_3) => {
                    cmdToTimer(dispatch_3);
                })];
            }
            else {
                return [state, Cmd_none()];
            }
        }
        default: {
            const t = state.fields[0] | 0;
            const p = state.fields[1] | 0;
            if ((t === 0) ? true : CheckForWin()) {
                return [new State(2, [p]), Cmd_none()];
            }
            else {
                switch (msg.tag) {
                    case 0:
                        return [new State(3, [t - 1, p]), Cmd_ofEffect((dispatch) => {
                            cmdToTimer(dispatch);
                        })];
                    case 1: {
                        const pic_id = msg.fields[0] | 0;
                        item(pic_id, Pictures()).numpressed = ((item(pic_id, Pictures()).numpressed + 1) | 0);
                        return [new State(0, [t, pic_id, p]), Cmd_none()];
                    }
                    default:
                        return [state, Cmd_none()];
                }
            }
        }
    }
}

export function picDisplay(id, state, dispatch) {
    const pic = item(id, Pictures());
    return createElement("img", createObj(toList(delay(() => append((pic.paired === false) ? append(singleton_1(["style", {
        margin: 10,
        width: 110,
        height: 110,
        visibility: "visible",
    }]), delay(() => singleton_1(["onClick", (me) => {
        dispatch(new Message(1, [id]));
    }]))) : singleton_1(["style", {
        margin: 10,
        width: 110,
        height: 110,
        visibility: "hidden",
    }]), delay(() => {
        let pic_id1, pic_id2, pic_id1_1;
        return singleton_1(["src", (state.tag === 3) ? (pic.paired ? "" : "images/test.png") : ((state.tag === 0) ? ((pic_id1 = (state.fields[1] | 0), pic.paired ? "" : ((pic_id1 === id) ? pic.pic_name : "images/test.png"))) : ((state.tag === 1) ? ((pic_id2 = (state.fields[2] | 0), (pic_id1_1 = (state.fields[1] | 0), pic.paired ? "" : (((pic_id1_1 === id) ? true : (pic_id2 === id)) ? pic.pic_name : "images/test.png")))) : ""))]);
    }))))));
}

export function display(state, t, p, dispatch) {
    let elems_3, elems, elems_2, elems_1;
    return createElement("div", createObj(ofArray([["style", {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    }], (elems_3 = [createElement("div", createObj(ofArray([["style", {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "#2B304F",
        borderStyle: "double",
        borderColor: "white",
        borderWidth: 6,
        borderRadius: 5,
        width: 400,
        height: 100,
        color: "white",
        fontFamily: "Georgia",
        fontSize: 25 + "px",
    }], (elems = [createElement("label", {
        children: "Preostalo vremena: " + int32ToString(t),
    }), createElement("label", {
        children: "Osvojeni bodovi: " + int32ToString(p),
    })], ["children", Interop_reactApi.Children.toArray(Array.from(elems))])]))), createElement("br", {}), createElement("div", createObj(ofArray([["style", {
        width: 600,
        height: 500,
        justifyContent: "center",
        display: "flex",
        alignItems: "center",
    }], (elems_2 = [createElement("div", createObj(singleton_2((elems_1 = [picDisplay(0, state, dispatch), picDisplay(1, state, dispatch), picDisplay(2, state, dispatch), picDisplay(3, state, dispatch), createElement("br", {}), picDisplay(4, state, dispatch), picDisplay(5, state, dispatch), picDisplay(6, state, dispatch), picDisplay(7, state, dispatch), createElement("br", {}), picDisplay(8, state, dispatch), picDisplay(9, state, dispatch), picDisplay(10, state, dispatch), picDisplay(11, state, dispatch), createElement("br", {})], ["children", Interop_reactApi.Children.toArray(Array.from(elems_1))]))))], ["children", Interop_reactApi.Children.toArray(Array.from(elems_2))])])))], ["children", Interop_reactApi.Children.toArray(Array.from(elems_3))])])));
}

export function GameOverDisplayOnly(state, p, dispatch) {
    let elems;
    return createElement("div", createObj(ofArray([["style", {
        alignItems: "center",
        justifyContent: "center",
        display: "flex",
    }], (elems = [createElement("div", {
        style: {
            justifyContent: "center",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            backgroundColor: "#2B304F",
            borderStyle: "double",
            borderColor: "white",
            borderWidth: 6,
            borderRadius: 5,
            margin: 100,
            width: 450,
            height: 80,
            color: "white",
            fontFamily: "Georgia",
            fontSize: 35 + "px",
        },
        children: "Ostvareni bodovi: " + int32ToString(p),
    }), createElement("br", {}), createElement("br", {}), createElement("button", {
        style: {
            justifyContent: "center",
            display: "flex",
            alignItems: "center",
            backgroundColor: "#2B304F",
            color: "white",
            fontFamily: "Georgia",
            borderRadius: 15,
            fontSize: 20 + "px",
            height: 90,
            width: 150,
        },
        children: "Zaigraj ponovo",
        onClick: (me) => {
            dispatch(new Message(2, []));
        },
    })], ["children", Interop_reactApi.Children.toArray(Array.from(elems))])])));
}

export function render(state, dispatch) {
    switch (state.tag) {
        case 0: {
            const t_1 = state.fields[0] | 0;
            const pic_id1 = state.fields[1] | 0;
            const p_1 = state.fields[2] | 0;
            return display(state, t_1, p_1, dispatch);
        }
        case 1: {
            const t_2 = state.fields[0] | 0;
            const pic_id2 = state.fields[2] | 0;
            const pic_id1_1 = state.fields[1] | 0;
            const p_2 = state.fields[3] | 0;
            return display(state, t_2, p_2, dispatch);
        }
        case 2: {
            const p_3 = state.fields[0] | 0;
            return GameOverDisplayOnly(state, p_3, dispatch);
        }
        default: {
            const t = state.fields[0] | 0;
            const p = state.fields[1] | 0;
            return display(state, t, p, dispatch);
        }
    }
}

ProgramModule_run(Program_withReactSynchronous("app", ProgramModule_mkProgram(init, update, render)));

